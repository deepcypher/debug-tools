FROM alpine:edge

RUN apk --no-cache add \
    net-tools \
    iputils \
    openssl \
    ldns \
    bind \
    netcat-openbsd \
    wireshark \
    socat \
    go \
    wget \
    curl \
    ethtool \
    strace
