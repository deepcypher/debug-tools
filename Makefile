REGISTRY="registry.gitlab.com"
IMAGE="deepcypher/debug-tools"
# short and fallback versioning
TAG=$(shell git describe --abbrev=0 || printf "r%s.%s" "$(git rev-list --count HEAD)" "$(git rev-parse --short HEAD)")

.PHONY: all
all: build

.PHONY: build
build:
	@sudo podman build -t "${REGISTRY}/${IMAGE}:${TAG}" -f Dockerfile .

.PHONY: run
run: build
	@podman run -p 127.0.0.1:38080:8080 -it "${REGISTRY}/${IMAGE}:${TAG}"

.PHONY: push
push: login build
	@podman push "${REGISTRY}/${IMAGE}:${TAG}"

.PHONY: login
login: login.lock

login.lock:
	@podman login ${REGISTRY}
	@touch login.lock

.PHONY: clean
clean:
	@rm -f login.lock
	@podman rmi "${REGISTRY}/${IMAGE}:${TAG}"
